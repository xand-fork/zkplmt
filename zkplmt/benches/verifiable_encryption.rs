use criterion::{black_box, criterion_group, Criterion};
use curve25519_dalek::constants::RISTRETTO_BASEPOINT_POINT;
use rand::rngs::OsRng;
use zkplmt::core::get_random_curve_point;
use zkplmt::models::CurveVector;
use zkplmt::verifiable_encryption::*;

const G: RistrettoPoint = RISTRETTO_BASEPOINT_POINT;

criterion_group!(
    verifiable_encryption,
    successful_verification,
    bad_public_key_verification
);

#[allow(non_snake_case)]
fn successful_verification(c: &mut Criterion) {
    let mut csprng: OsRng = OsRng::default();
    let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
        .map(|_| CurveVector {
            x: get_random_curve_point(&mut csprng),
            y: get_random_curve_point(&mut csprng),
        })
        .collect();
    let signer_public_perm_P = get_random_curve_point(&mut csprng);
    let ve = VerifiableEncryptionOfCurvePoint::create(
        &signer_public_perm_P,
        &target_ephemeral_pub_A_B,
        &G,
        &mut csprng,
    )
    .expect("Failed to create proof");
    let description = "Verifying a valid verifiable encryption";
    c.bench_function(description, |b| {
        b.iter(|| ve.verify(black_box(&target_ephemeral_pub_A_B), &G))
    });
}

#[allow(non_snake_case)]
fn bad_public_key_verification(c: &mut Criterion) {
    let mut csprng: OsRng = OsRng::default();
    let target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
        .map(|_| CurveVector {
            x: get_random_curve_point(&mut csprng),
            y: get_random_curve_point(&mut csprng),
        })
        .collect();

    let different_target_ephemeral_pub_A_B: Vec<CurveVector> = (1..5)
        .map(|_| CurveVector {
            x: get_random_curve_point(&mut csprng),
            y: get_random_curve_point(&mut csprng),
        })
        .collect();
    let signer_public_perm_P = get_random_curve_point(&mut csprng);
    let ve = VerifiableEncryptionOfCurvePoint::create(
        &signer_public_perm_P,
        &target_ephemeral_pub_A_B,
        &G,
        &mut csprng,
    )
    .expect("Failed to create proof");
    let description = "Verifying a valid verifiable encryption";
    c.bench_function(description, |b| {
        b.iter(|| ve.verify(black_box(&different_target_ephemeral_pub_A_B), &G))
    });
}
