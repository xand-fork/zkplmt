# zkplmt

This library contains core cryptographic primitives, and a zero-knowledge proof
system. This is primarily consumed by 
[xand_ledger](https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger)
for the purpose of creating and verifying confidential transactions.

## Contributing

Changes to this repo need to be reviewed by a cryptography expert.
Nearly all the code was setup by Debasish as an implementation of the
zkplmt defined in the yellow paper.

